import 'dart:async';

import 'package:Covid_Kit/Models/ResponseTopCovidNews/response_top_covid_news.dart';
import 'package:Covid_Kit/services/news_api.dart';

class ApiRepository {
  final _apiProvider = ApiProvider();

  Future<ResponseTopCovidNews> fetchTopCovidNews() =>
      _apiProvider.getTopHeadlinesNews();

}