import 'package:Covid_Kit/login_api/src/models/login_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geoflutterfire/geoflutterfire.dart';

class Api {
  Future<String> signUpWithEmailAndPassword(
      String email,
      String password,
      String name,
      String number,
      GeoFirePoint myLocation,
      bool needs,
      String desc,
      String image) async {
    String error = "";
    UserUpdateInfo updateInfo = UserUpdateInfo();
    updateInfo.displayName = name;

    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((user) async {
        await user.user.updateProfile(updateInfo);
        await user.user.reload();

        Firestore.instance.collection('Users').document(user.user.uid).setData({
          "id": user.user.uid,
          "email": email,
          "name": name,
          "description": desc,
          "image": image,
          "created_at": DateTime.now(),
          "number": number,
          "location": myLocation.data,
          "case": needs,
        });
      });
    } catch (e) {
      print(e.toString());
      error = "Error encountered!";
    }

    return error;
  }

  Future<String> signInWithEmailAndPassword(LoginData loginData) async {
    String error = "";

    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
          email: loginData.name, password: loginData.password);
    } catch (e) {
      error = "Error, try again!";
    }

    return error;
  }

  Future<FirebaseUser> getUserProfile() => FirebaseAuth.instance.currentUser();
}
