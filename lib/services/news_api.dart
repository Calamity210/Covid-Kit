import 'package:Covid_Kit/Models/ResponseTopCovidNews/response_top_covid_news.dart';
import 'package:dio/dio.dart';

class ApiProvider {
  final Dio _dio = Dio();
  final String _baseUrl =
      'http://newsapi.org/v2/everything?q=Covid-19&sortBy=popularity&apiKey=f54c8afabba24c14b0c90b28b7733875';

  void printOutError(error, StackTrace stacktrace) {
    print('Exception occured: $error with stacktrace: $stacktrace');
  }

  Future<ResponseTopCovidNews> getTopHeadlinesNews() async {
    try {
      final response = await _dio.get(_baseUrl);
      return ResponseTopCovidNews.fromJson(response.data);
    } catch (error, stacktrace) {
      printOutError(error, stacktrace);
      return ResponseTopCovidNews.withError('$error');
    }
  }

  
}