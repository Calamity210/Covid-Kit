import 'package:flutter/material.dart';

enum MyThemeKeys {LIGHT, DARK}



class MyThemes {

 static Map<int, Color> color =
{
50:Color.fromRGBO(136,14,79, .1),
100:Color.fromRGBO(136,14,79, .2),
200:Color.fromRGBO(136,14,79, .3),
300:Color.fromRGBO(136,14,79, .4),
400:Color.fromRGBO(136,14,79, .5),
500:Color.fromRGBO(136,14,79, .6),
600:Color.fromRGBO(136,14,79, .7),
700:Color.fromRGBO(136,14,79, .8),
800:Color.fromRGBO(136,14,79, .9),
900:Color.fromRGBO(136,14,79, 1),
};

static MaterialColor purple = MaterialColor(0xFF3700B3, color);

static MaterialColor green = MaterialColor(0xFF9DF62F, color);

static MaterialColor darkBlue = MaterialColor(0xFF12202F, color);


  static final ThemeData lightTheme = ThemeData(
    primaryColor: Color(0xFF3700B3),
    backgroundColor: Colors.grey.shade100,
    hintColor: Colors.black,
    brightness: Brightness.light,
  );

  static final ThemeData darkTheme = ThemeData(
    primaryColor: Color(0xFF3700B3),
    backgroundColor: Color.fromARGB(255, 18, 32, 47),
    hintColor: Colors.white,
    brightness: Brightness.dark,
  );

  static ThemeData getThemeFromKey(MyThemeKeys themeKey) {
    switch (themeKey) {
      case MyThemeKeys.LIGHT:
        return lightTheme;
      case MyThemeKeys.DARK:
        return darkTheme;
      default:
        return darkTheme;
    }
  }
}