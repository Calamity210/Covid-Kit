import 'package:Covid_Kit/Models/login_model.dart';
import 'package:Covid_Kit/Models/signup_model.dart';
import 'package:Covid_Kit/login_api/authentication_service.dart';
import 'package:Covid_Kit/services/api.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => LoginModel());
  locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton<SignModel>(() => new SignModel());
}
