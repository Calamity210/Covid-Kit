// TabNine::sem
import 'package:Covid_Kit/themes/theme_widget.dart';
import 'package:Covid_Kit/themes/themes.dart';
import 'package:Covid_Kit/ui/dashboard_page.dart';
import 'package:Covid_Kit/ui/login_view.dart';
import 'package:Covid_Kit/locator.dart';
import 'package:custom_splash/custom_splash.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_stetho/flutter_stetho.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isDark = prefs.getBool("isDark") ?? false;
  setupLocator();
  Stetho.initialize();

  runApp(
    MaterialApp(
      home:  CustomSplash(
        imagePath: 'assets/splash.png',
        backGroundColor: Color(0XFF3700B3),
        animationEffect: 'zoom-in',
        logoSize: 75,
        home: CustomTheme(
            initialThemeKey: isDark ? MyThemeKeys.DARK : MyThemeKeys.LIGHT,
            child: SpiDev(),
          ),
        duration: 2500,
        type: CustomSplashType.StaticDuration,
    ),
    ),
  );
}

class SpiDev extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "SpiDev",
      theme: CustomTheme.of(context),
      home: FutureBuilder<FirebaseUser>(
          future: FirebaseAuth.instance.currentUser(),
          builder:
              (BuildContext context, AsyncSnapshot<FirebaseUser> snapshot) {
            if (snapshot.hasData) {
              // this is your user instance
              /// user already logged
              ///
              return Dashboard();
            }

            /// there is no user logged in.
            return LoginView();
          }),
      //MainPage(),
    );
  }
}
