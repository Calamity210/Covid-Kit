import 'package:Covid_Kit/login_api/src/models/login_data.dart';
import 'package:Covid_Kit/services/api.dart';
import 'package:Covid_Kit/locator.dart';
import 'package:geoflutterfire/geoflutterfire.dart';


class AuthenticationService {
  Api _api = locator<Api>();

  Future<bool> signUp(String email, String password, String name, String number,
  GeoFirePoint location, bool needs, String desc, String image ) async{

    _api.signUpWithEmailAndPassword(email, password, name, number, location, needs, desc, image);
    var fetchedUser = await _api.getUserProfile();

    return fetchedUser != null;
  } 

  Future<bool> login(LoginData loginData) async{

    _api.signInWithEmailAndPassword(loginData);

    var fetchedUser = await _api.getUserProfile();

    return fetchedUser != null;
  }
}