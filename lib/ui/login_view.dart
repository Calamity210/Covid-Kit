import 'package:Covid_Kit/Models/login_model.dart';
import 'package:Covid_Kit/locator.dart';
import 'package:Covid_Kit/login_api/flutter_login.dart';
import 'package:Covid_Kit/responsive/orientation.dart';
import 'package:Covid_Kit/responsive/screen_type_layout.dart';
import 'package:Covid_Kit/themes/theme_widget.dart';
import 'package:Covid_Kit/themes/themes.dart';
import 'package:Covid_Kit/ui/complete_signup.dart';
import 'package:Covid_Kit/ui/dashboard_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

GlobalKey<ScaffoldState> _scafKey = GlobalKey<ScaffoldState>();
GlobalKey<ScaffoldState> _scafKey1 = GlobalKey<ScaffoldState>();

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: LoginPage(),
        landscape: _LoginLandscape(),
      ),
      tablet: LoginPage(),
    );
  }
}

class _LoginLandscape extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginLandScapePageState();
}

class _LoginLandScapePageState extends State<_LoginLandscape> {
  void _changeTheme(BuildContext buildContext, MyThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;

    return ChangeNotifierProvider<LoginModel>.value(
      value: locator<LoginModel>(),
      child: Consumer<LoginModel>(
        builder: (context, model, child) => Scaffold(
          key: _scafKey,
          resizeToAvoidBottomInset: false,
          body: Container(
            color: Theme.of(context).backgroundColor,
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlutterLogin(
                    title: 'SpiDev',
                    theme: LoginTheme(
                      pageColorDark: Theme.of(context).backgroundColor,
                      pageColorLight: Theme.of(context).backgroundColor,
                      primaryColor: Theme.of(context).primaryColor,
                      buttonStyle:
                          Theme.of(context).brightness == Brightness.dark
                              ? TextStyle(color: Colors.white)
                              : null,
                      titleStyle: TextStyle(
                          fontFamily: "FLORLRG",
                          color: Theme.of(context).primaryColor),
                    ),
                    onLogin: (LoginData data) async {
                      var loginSuccess = await model.login(data);
                      if (loginSuccess) {
                        Navigator.of(context).pop();
                        Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              Dashboard(),
                        ),
                      );
                        return 'Success';
                      }

                      else{
                      _scafKey.currentState.showSnackBar(SnackBar(content: Text("Please check email & password and try again."),));
                      
                      return 'Failed';
                      }
                    },
                    onSignup: (LoginData data) async {
                      Navigator.of(context).pop();
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              CompleteSignupPage(data.name, data.password),
                        ),
                      );

                      return "Success";
                    },
                    onRecoverPassword: (String email) async {
                      String error = "";
                      await FirebaseAuth.instance
                          .sendPasswordResetEmail(email: email);
                      return error;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, right: 10),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      icon: Icon(FontAwesomeIcons.solidMoon),
                      onPressed: () async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        if (Theme.of(context).brightness == Brightness.light) {
                          prefs.setBool("isDark", true);
                          _changeTheme(context, MyThemeKeys.DARK);
                        } else {
                          prefs.setBool("isDark", false);
                          _changeTheme(context, MyThemeKeys.LIGHT);
                        }
                      },
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: RotatedBox(
                    quarterTurns: 1,
                    child: Container(
                      height: height * 0.4,
                      color: Colors.transparent,
                      child: WaveWidget(
                        config: CustomConfig(
                          gradients: [
                            [Colors.purple, Color(0xFF3700B3)],
                            [Colors.purpleAccent, Color(0xFF3700B3)],
                            [Colors.deepPurple, Color(0xFF3700B3)],
                            [Colors.deepPurpleAccent, Color(0xFF3700B3)]
                          ],
                          durations: [35000, 19440, 10800, 6000],
                          heightPercentages: [0.20, 0.30, 0.35, 0.40],
                          gradientBegin: Alignment.bottomLeft,
                          gradientEnd: Alignment.topRight,
                        ),
                        backgroundColor: Colors.transparent,
                        size: Size(
                            MediaQuery.of(context).size.height, height * 0.4),
                        waveAmplitude: 0,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  void _changeTheme(BuildContext buildContext, MyThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<LoginModel>.value(
      value: locator<LoginModel>(),
      child: Consumer<LoginModel>(
        builder: (context, model, child) => Scaffold(
          key: _scafKey1,
          resizeToAvoidBottomInset: false,
          body: Container(
            child: Stack(
              children: <Widget>[
                FlutterLogin(
                  title: 'Covid-Kit',
                  theme: LoginTheme(
                    pageColorDark: Theme.of(context).backgroundColor,
                    pageColorLight: Theme.of(context).backgroundColor,
                    primaryColor: Theme.of(context).primaryColor,
                    buttonStyle: Theme.of(context).brightness == Brightness.dark
                        ? TextStyle(color: Colors.white)
                        : null,
                    titleStyle: TextStyle(
                        fontFamily: "FLORLRG",
                        color: Theme.of(context).primaryColor),
                  ),
                  onLogin: (LoginData data) async {
                    var loginSuccess = await model.login(data);
                    if (loginSuccess) {
                      Navigator.of(context).pop();
                        Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              Dashboard(),
                        ),
                      );
                        return 'Success';
                      }

                       else{
                      _scafKey1.currentState.showSnackBar(SnackBar(content: Text("Please check email & password and try again."),));
                      
                      return 'Failed';
                      }
                  },
                  onSignup: (LoginData data) async {
                    Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            CompleteSignupPage(data.name, data.password),
                      ),
                    );

                    return "Success";
                  },
                  onRecoverPassword: (String email) async {
                    String error = "";
                    await FirebaseAuth.instance
                        .sendPasswordResetEmail(email: email);
                    return error;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0, right: 5),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      icon: Icon(FontAwesomeIcons.solidMoon),
                      onPressed: () async {
                        SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        if (Theme.of(context).brightness == Brightness.light) {
                          prefs.setBool("isDark", true);
                          _changeTheme(context, MyThemeKeys.DARK);
                        } else {
                          prefs.setBool("isDark", false);
                          _changeTheme(context, MyThemeKeys.LIGHT);
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
