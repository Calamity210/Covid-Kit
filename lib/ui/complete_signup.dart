import 'dart:io';

import 'package:Covid_Kit/Models/signup_model.dart';
import 'package:Covid_Kit/locator.dart';
import 'package:Covid_Kit/ui/custom_textfield.dart';
import 'package:Covid_Kit/ui/dashboard_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flare_loading/flare_loading.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:location/location.dart' as locationPackage;
import 'package:provider/provider.dart';
import 'package:path/path.dart';

final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
final GlobalKey<ScaffoldState> _scafKey = GlobalKey<ScaffoldState>();
bool _autoValidate = false;

class CompleteSignupPage extends StatefulWidget {
  final email;
  final password;


  CompleteSignupPage(this.email, this.password);
  @override
  State<StatefulWidget> createState() => _CompleteSignupPageState();
}

class _CompleteSignupPageState extends State<CompleteSignupPage> {
   locationPackage.Location _locationService = new locationPackage.Location();
     bool _permission = false;

  Geoflutterfire geo;
  GeoFirePoint myLocation;

  File _image;

  String _name, _desc, _number, _imageUrl;

  bool need = false;
  bool enabled = true;
  //  bool _serviceEnabled;

  // Future<void> _checkPermissions() async {
  //   final PermissionStatus permissionGrantedResult =
  //       await location.hasPermission();
  //   if (permissionGrantedResult != PermissionStatus.GRANTED) {
  //     _requestPermission();
  //   } else {
  //     await _getLocation();
  //   }
  // }

  // _getLocation() async {
  //   try {
  //     final LocationData _locationResult = await location.getLocation();
  //     setState(() {
  //       var locData = _locationResult;
  //       myLocation =
  //           geo.point(latitude: locData.latitude, longitude: locData.longitude);
  //     });
  //   } catch (e) {
  //     print(("-" * 60) + e.toString());
  //   }
  // }


  // Future<void> _checkService() async {
  //   final bool serviceEnabledResult = await location.serviceEnabled();
  //   if(!serviceEnabledResult) _requestService();
  //   else _getLocation();
  //   setState(() {
  //     _serviceEnabled = serviceEnabledResult;
  //   });
  // }

  // Future<void> _requestService() async {
  //   if (_serviceEnabled == null || !_serviceEnabled) {
  //     final bool serviceRequestedResult = await location.requestService();
  //     setState(() {
  //       _serviceEnabled = serviceRequestedResult;
  //     });
  //     if (!serviceRequestedResult) {
  //       _requestService();

  //     } else _getLocation();
  //   }
  // }

  // _requestPermission() async {
  //   final PermissionStatus permissionRequestedResult =
  //       await location.requestPermission();

  //   if (permissionRequestedResult != PermissionStatus.GRANTED) {
  //     _requestPermission();
  //   }
  //   else{
  //     _checkService();
  //     _getLocation();
  //   }

  // }

  fetchCurrentLocation() async {
    await _locationService.changeSettings(
        accuracy: locationPackage.LocationAccuracy.HIGH, interval: 1000);

    locationPackage.LocationData location;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      bool serviceStatus = await _locationService.serviceEnabled();
      print("Service status: $serviceStatus");
      if (serviceStatus) {
        _permission = await _locationService.requestPermission() == locationPackage.PermissionStatus.GRANTED;
        print("Permission: $_permission");
        if (_permission) {
          location = await _locationService.getLocation();

          myLocation =
            geo.point(latitude: location.latitude, longitude: location.longitude);

          print("Location: ${location.latitude}");
        }
      } else {
        bool serviceStatusResult = await _locationService.requestService();
        print("Service status activated after request: $serviceStatusResult");
        if (serviceStatusResult) {
          fetchCurrentLocation();
        }
      }
    } catch (e) {
      print(e);
      if (e.code == 'PERMISSION_DENIED') {
        //error = e.message;
      } else if (e.code == 'SERVICE_STATUS_ERROR') {
        //error = e.message;
      }
      location = null;
    }
  }

  @override
  void initState() {
    super.initState();
    geo = Geoflutterfire();
    fetchCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    void _validateRegisterInput() async {
      final FormState form = _formKey.currentState;
      if (_formKey.currentState.validate()) {
        form.save();
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

    Future<bool> detectFaces() async {
      FirebaseVisionImage _img;
      final data = await _image.readAsBytes();
      await decodeImageFromList(data).then(
        (value) => setState(() {
          _img = FirebaseVisionImage.fromFile(_image);
        }),
      );

      final faceDetector = FirebaseVision.instance.faceDetector();
      List<Face> faces = await faceDetector.processImage(_img);

      return faces.isNotEmpty;
    }

    Future getImage() async {
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);

      setState(() {
        _image = image;
        print('Image Path $_image');
      });
    }

    Future uploadPic(BuildContext context) async {
      String fileName = basename(_image.path);
      StorageReference firebaseStorageRef =
          FirebaseStorage.instance.ref().child(fileName);
      StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
      StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
      var url = await taskSnapshot.ref.getDownloadURL();
      _imageUrl = url.toString();
      setState(() {
        print("Profile Picture uploaded");
        _scafKey.currentState
            .showSnackBar(SnackBar(content: Text('Profile Picture Uploaded')));
      });
    }

    Widget filledButton(String text, Color splashColor, Color highlightColor,
        Color fillColor, Color textColor, void function(), SignModel model) {
      return RaisedButton(
        highlightElevation: 0.0,
        splashColor: splashColor,
        highlightColor: highlightColor,
        
        elevation: 0.0,
        color: fillColor,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0)),
        child: Text(
          text,
          style: TextStyle(
              fontWeight: FontWeight.bold, color: textColor, fontSize: 20),
        ),
        onPressed: () async {
          bool isFace = await detectFaces();
          if (isFace && myLocation != null && need != null) {
           enabled = false;

            await uploadPic(context).then((_) async {
              await model
                  .signup(widget.email, widget.password, _name, _number,
                      myLocation, need, _desc, _imageUrl)
                  .then((_) async {
                await FirebaseAuth.instance.currentUser().then((user) {
                    enabled = true;
                  Navigator.of(context).pop();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Dashboard(),
                      ),
                    );
                });
              });
            });
          } else {
            if (!isFace)
              _scafKey.currentState.showSnackBar(
                  SnackBar(content: Text('No face detected in picture')));
            else if (myLocation == null)
              _scafKey.currentState.showSnackBar(
                SnackBar(
                  content: Text("Can't get location"),
                ),
              );
            else
              _scafKey.currentState.showSnackBar(
                SnackBar(
                  content: Text("No field can be left empty"),
                ),
              );
          }
        },
      );
    }

    String nameValidator(String value) {
      if (value.isEmpty) return '*Required';
      if (!value.contains(" "))
        return '*Enter your Full Name';
      else
        return null;
    }

    return ChangeNotifierProvider<SignModel>.value(
      value: locator<SignModel>(),
      child: Consumer<SignModel>(
        builder: (context, model, child) => Scaffold(
          key: _scafKey,
          appBar: AppBar(
            leading: IconButton(
                icon: Icon(FontAwesomeIcons.arrowLeft),
                onPressed: () {
                  Navigator.pop(context);
                }),
            title: Text('Finish Signup'),
          ),
          resizeToAvoidBottomInset: false,
          body: Builder(
            builder: (context) => SingleChildScrollView(
              child: Container(
                color: Theme.of(context).backgroundColor,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: CircleAvatar(
                            radius: 100,
                            backgroundColor: Theme.of(context).primaryColor,
                            child: ClipOval(
                              child: new SizedBox(
                                width: 190.0,
                                height: 190.0,
                                child: (_image != null)
                                    ? Image.file(
                                        _image,
                                        fit: BoxFit.fill,
                                      )
                                    : Image.network(
                                        "https://images.unsplash.com/photo-1502164980785-f8aa41d53611?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
                                        fit: BoxFit.fill,
                                      ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 60.0),
                          child: IconButton(
                            icon: Icon(
                              FontAwesomeIcons.camera,
                              size: 30.0,
                            ),
                            onPressed: () {
                              getImage();
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Form(
                      key: _formKey,
                      autovalidate: _autoValidate,
                      child: Column(
                        children: <Widget>[
                          CustomTextField(
                            onSaved: (input) {
                              _name = input;
                            },
                            validator: nameValidator,
                            icon: Icon(Icons.person),
                            hint: "Name",
                          ),
                          SizedBox(height: 20),
                          CustomTextField(
                            onSaved: (input) {
                              _desc = input;
                            },
                            validator: (input) => input == null ?? "*Required",
                            icon: Icon(Icons.description),
                            hint: "Description",
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          InternationalPhoneNumberInput(
                            onInputChanged: (PhoneNumber number) {
                              _number = number.toString();
                            },
                            isEnabled: true,
                            autoValidate: true,
                            formatInput: true,
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          RadioButtonGroup(
                            labels: [
                              "Need help",
                              "Want to help",
                            ],
                            //    disabled: ["Option 1"],
                            onChange: (String label, int index) {
                              label == "Need help" ? need = true : need = false;
                            },

                            onSelected: (String label) => print(label),
                          ),
                          SizedBox(
                            height: 20.0,
                          ),
                          !enabled
                              ? FlareLoading(
                                  name: 'assets/loading.flr',
                                  //loopAnimation: 'Loading',
                                  startAnimation: 'Untitled',
                                  endAnimation: 'Untitled',
                                  width: 50,
                                  height: 50,
                                  fit: BoxFit.fill,
                                  until: () =>
                                      Future.delayed(Duration(seconds: 5)),
                                  onSuccess: (_) {
                                    print('Finished');
                                  },
                                  onError: (err, stack) {
                                    print(err);
                                  },
                                )
                              : filledButton(
                                  "REGISTER",
                                  Colors.white,
                                  Theme.of(context).primaryColor,
                                  Theme.of(context).primaryColor,
                                  Colors.white,
                                  _validateRegisterInput,
                                  model,
                                ),
                          SizedBox(
                            height: 20.0,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

/* var signupSuccess = await model.signup(data);
                      if (signupSuccess) {
                        // navigate to pageColorLightr
                        return 'Success';
                      }
                      return 'Failed';
                      */
