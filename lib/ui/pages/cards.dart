import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flare_loading/flare_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:great_circle_distance2/great_circle_distance2.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:swipe_stack/swipe_stack.dart';

class CardPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CardPageState();
}

class _CardPageState extends State<CardPage> {
  FirebaseUser fuser;
  double curLat;
  double curLong;
  bool needsHelp;
  String curName;
  String desc;

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  _getUser() async {
    fuser = await FirebaseAuth.instance
        .currentUser()
        .then((user) => _setUser(user));
  }

  _setUser(user) async {
    await Firestore.instance
        .collection("Users")
        .document(user.uid)
        .get()
        .then((snap) {
      setState(() {
        GeoPoint point = snap.data['location']['geopoint'];
        curLat = point.latitude;
        curLong = point.longitude;
        needsHelp = snap.data['case'];
        curName = snap.data['name'];
        desc = snap.data['description'];
      });
    });
  }

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {});
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    setState(() {});
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(milliseconds: 2000));
     return Container(
      color: Theme.of(context).backgroundColor,
      child: Stack(
        children: <Widget>[
          SmartRefresher(
            enablePullDown: true,
            enablePullUp: false,
            header: WaterDropMaterialHeader(
              color: Theme.of(context).primaryColor,
            ),
            child: Container(
              color: Theme.of(context).backgroundColor,
              child: Center(
                child: Text("Whoops! We ran out of people!",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ),
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
          ),
          FutureBuilder(
              future: Firestore.instance.collection("Users").getDocuments(),
              builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  List<DocumentSnapshot> docSnap = snapshot.data.documents;

                  docSnap.retainWhere((item) {
                    GeoPoint point = item.data['location']['geopoint'];
                    print(curLat);
                    print(curLong);
                    print(point.latitude);
                    print(point.longitude);

                    var gcd = GreatCircleDistance.fromDegrees(
                      latitude1: curLat,
                      longitude1: curLong,
                      latitude2: point.latitude,
                      longitude2: point.longitude,
                    );

                    return (gcd.vincentyDistance() <= 80000.0 &&
                        item["case"] != needsHelp);
                  });

                  docSnap.shuffle();

                  // docSnap.retainWhere((item) {‰
                  //   return item["case"] != needsHelp;
                  // });

                  return SwipeStack(
                    children: docSnap.map((DocumentSnapshot snap) {
                      GeoPoint point = snap.data['location']['geopoint'];

                      var gcd = GreatCircleDistance.fromDegrees(
                        latitude1: curLat,
                        longitude1: curLong,
                        latitude2: point.latitude,
                        longitude2: point.longitude,
                      );

                      var distance = gcd.vincentyDistance();

                      return SwiperItem(
                          builder: (SwiperPosition position, double progress) {
                        return Material(
                          elevation: 5,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              image: DecorationImage(
                                image: NetworkImage(
                                  snap.data['image'],
                                ),
                                fit: BoxFit.fill,
                              ),
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  height: 175,
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(25),
                                      topRight: Radius.circular(25),
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10),
                                    ),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          snap.data['name'],
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          (distance ~/ 1000).toString() +
                                              " Km(s) away",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          RaisedButton(
                                            child: Text("About"),
                                            elevation: 5.0,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(25),
                                              ),
                                            ),
                                            color: Colors.blue,
                                            onPressed: () {
                                              showDialog(
                                                barrierDismissible: true,
                                                builder: (context) {
                                                  return AlertDialog(
                                                    title: Text("About Me"),
                                                    content: Text(
                                                      snap.data['description'],
                                                    ),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                          child: Text("Ok"),
                                                          onPressed: () {
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          }),
                                                    ],
                                                  );
                                                },
                                                context: context,
                                              );
                                            },
                                          ),
                                          RaisedButton(
                                            child: needsHelp
                                                ? Text("Request Help")
                                                : Text("Offer Help"),
                                            elevation: 5.0,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(25),
                                              ),
                                            ),
                                            color: Colors.lightGreenAccent,
                                            onPressed: () {
                                              String txt = "";
                                              needsHelp
                                                  ? showDialog(
                                                      barrierDismissible: true,
                                                      builder: (context) {
                                                        return AlertDialog(
                                                          title: Text(
                                                              "Request Help"),
                                                          content: Container(
                                                            child: Center(
                                                              child: TextField(
                                                                autocorrect:
                                                                    true,
                                                                autofocus: true,
                                                                keyboardType:
                                                                    TextInputType
                                                                        .text,
                                                                onChanged:
                                                                    (input) =>
                                                                        txt =
                                                                            input,
                                                                decoration:
                                                                    InputDecoration(
                                                                  hintText:
                                                                      "Briefly explain what you need",
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          actions: <Widget>[
                                                            FlatButton(
                                                                child:
                                                                    Text("SMS"),
                                                                onPressed:
                                                                    () async {
                                                                  String msg =
                                                                      """
                                                                      Hey, please let me know if you can help me with $txt. I can provide more details on request. 

                                                                      Thanks,
$curName
                                                                      """;

                                                                  await sendSMS(
                                                                    message:
                                                                        msg,
                                                                    recipients: [
                                                                      snap.data[
                                                                          "number"],
                                                                    ],
                                                                  );
                                                                }),
                                                            FlatButton(
                                                              child:
                                                                  Text("Email"),
                                                              onPressed:
                                                                  () async {
                                                                String msg = """
                                                                      Hey, please let me know if you can help me with $txt. I can provide more details on request. 

                                                                      Thanks,
$curName
                                                                      """;
                                                                final Email
                                                                    email =
                                                                    Email(
                                                                  body: msg,
                                                                  subject:
                                                                      'Covid-Kit Assistance Requested',
                                                                  recipients: [
                                                                    snap.data[
                                                                        'email'],
                                                                  ],
                                                                  isHTML: false,
                                                                );

                                                                await FlutterEmailSender
                                                                    .send(
                                                                        email);
                                                              },
                                                            ),
                                                          ],
                                                        );
                                                      },
                                                      context: context,
                                                    )
                                                  : showDialog(
                                                      barrierDismissible: true,
                                                      builder: (context) {
                                                        return AlertDialog(
                                                          title: Text(
                                                              "Offer Help"),
                                                          content: Text(
                                                            snap.data[
                                                                'description'],
                                                          ),
                                                          actions: <Widget>[
                                                            FlatButton(
                                                                child:
                                                                    Text("SMS"),
                                                                onPressed:
                                                                    () async {
                                                                  String msg =
                                                                      """
                                                                      Hey! I wanted to check in and see if you required any assistance. Here is a little about me, $desc.
                                                                      Thanks,
$curName
                                                                      """;

                                                                  await sendSMS(
                                                                    message:
                                                                        msg,
                                                                    recipients: [
                                                                      snap.data[
                                                                          "number"],
                                                                    ],
                                                                  );
                                                                }),
                                                            FlatButton(
                                                              child:
                                                                  Text("Email"),
                                                              onPressed:
                                                                  () async {
                                                                String msg = """
                                                                      Hey! I wanted to check in and see if you required any assistance. Here is a little about me, $desc.
                                                                      Thanks,
$curName
                                                                      """;
                                                                final Email
                                                                    email =
                                                                    Email(
                                                                  body: msg,
                                                                  subject:
                                                                      'Covid-Kit Assistance Offer',
                                                                  recipients: [
                                                                    snap.data[
                                                                        'email'],
                                                                  ],
                                                                  isHTML: false,
                                                                );

                                                                await FlutterEmailSender
                                                                    .send(
                                                                        email);
                                                              },
                                                            ),
                                                          ],
                                                        );
                                                      },
                                                      context: context,
                                                    );
                                            },
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      });
                    }).toList(),
                    visibleCount: 3,
                    stackFrom: StackFrom.Top,
                    translationInterval: 6,
                    scaleInterval: 0.03,
                    onEnd: () => debugPrint("onEnd"),
                    onSwipe: (int index, SwiperPosition position) {
                      debugPrint("onSwipe $index $position");
                    },
                    onRewind: (int index, SwiperPosition position) =>
                        debugPrint("onRewind $index $position"),
                  );
                } else {
                  return Center(
                      child: FlareLoading(
                    name: 'assets/loading.flr',
                    //loopAnimation: 'Loading',
                    startAnimation: 'Untitled',
                    endAnimation: 'Untitled',
                    width: 200,
                    height: 200,
                    fit: BoxFit.fill,
                    isLoading: !snapshot.hasData,
                    onSuccess: (_) {
                      print('Finished');
                    },
                    onError: (err, stack) {
                      print(err);
                    },
                  ));
                }
              }),
        ],
      ),
    ); 
    
  }
}
