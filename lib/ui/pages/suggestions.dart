import 'package:Covid_Kit/ui/symptom_items.dart';
import 'package:flutter/material.dart';

class SuggestionsPage extends StatefulWidget {
  @override
  _SuggestionsPageState createState() => _SuggestionsPageState();
}

class _SuggestionsPageState extends State<SuggestionsPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(24.0),
            color: Theme.of(context).backgroundColor,
            child: Center(
              child: RichText(
                text: TextSpan(
                  style: DefaultTextStyle.of(context).style,
                  children: <TextSpan>[
                    TextSpan(
                      text:
                          "Stay aware of the latest information on the COVID-19 outbreak, available on the WHO website and through your national and local public health authority. COVID-19 is still affecting mostly people in China with some outbreaks in other countries. Most people who become infected experience mild illness and recover, but it can be more severe for others. Take care of your health and protect others by doing the following:",
                      style: Theme.of(context).textTheme.subhead,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            color: Theme.of(context).primaryColor,
            child: PreventionItems(),
          ),
          SizedBox(
            height: 28.0,
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 16.0,
            ),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(5.0)),
            child: FlatButton.icon(
              onPressed: () {
                launchUrl();
              },
              icon: Icon(Icons.open_in_new, color: Colors.white),
              label: Text(
                "WHO",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          SizedBox(
            height: 32.0,
          ),
        ],
      ),
    );
  }
}
