import 'package:Covid_Kit/Models/ResponseTopCovidNews/response_top_covid_news.dart';
import 'package:Covid_Kit/services/news_api_respository.dart';
import 'package:flare_loading/flare_loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

final GlobalKey<ScaffoldState> scaffoldState = GlobalKey<ScaffoldState>();

class NewsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  final apiRepository = ApiRepository();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 15.0),
          child: FutureBuilder(
            future: apiRepository.fetchTopCovidNews(),
            builder: (BuildContext context,
                AsyncSnapshot<ResponseTopCovidNews> snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: snapshot.data.articles.length,
                  itemBuilder: (context, i) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 15.0),
                      child: ListTile(
                        enabled: true,
                        leading:
                            Image.network(snapshot.data.articles[i].urlToImage),
                        trailing: Icon(Icons.keyboard_arrow_right),
                        title: Text(
                          snapshot.data.articles[i].title,
                          maxLines: 2,
                        ),
                        subtitle: Text(
                          snapshot.data.articles[i].description,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        isThreeLine: true,
                        onTap: () async {
                          await canLaunch(snapshot.data.articles[i].url)
                              .then((_) {
                            launch(snapshot.data.articles[i].url);
                          });
                        },
                      ),
                    );
                  },
                );
              } else {
                return Center(
                    child: FlareLoading(
                  name: 'assets/loading.flr',
                  //loopAnimation: 'Loading',
                  startAnimation: 'Untitled',
                  endAnimation: 'Untitled',
                  width: 200,
                  height: 200,
                  fit: BoxFit.fill,
                  isLoading: !snapshot.hasData,
                  onSuccess: (_) {
                    print('Finished');
                  },
                  onError: (err, stack) {
                    print(err);
                  },
                ));
              }
            },
          ),
        ),
        Align(
          alignment: Alignment.bottomRight,
          child: Text("Powered by News API"),
        ),
      ],
    );
  }
}
