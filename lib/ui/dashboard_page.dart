import 'package:Covid_Kit/themes/theme_widget.dart';
import 'package:Covid_Kit/themes/themes.dart';
import 'package:Covid_Kit/ui/login_view.dart';
import 'package:Covid_Kit/ui/pages/suggestions.dart';
import 'package:Covid_Kit/ui/pages/cards.dart';
import 'package:Covid_Kit/ui/pages/global_cases.dart';
import 'package:Covid_Kit/ui/pages/news.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  PageController _controller = PageController(
    initialPage: 0,
    keepPage: true,
  );
  bool needsHelp;
  double curLat;
  double curLong;
  FirebaseUser fuser;
  int bottomIndex = 0;

  void _changeTheme(BuildContext buildContext, MyThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  void _select(Choice choice) async {
    switch (choice.title) {
      case "Switch Theme":
        SharedPreferences prefs = await SharedPreferences.getInstance();
        if (Theme.of(context).brightness == Brightness.light) {
          prefs.setBool("isDark", true);
          _changeTheme(context, MyThemeKeys.DARK);
        } else {
          prefs.setBool("isDark", false);
          _changeTheme(context, MyThemeKeys.LIGHT);
        }
        break;
      case "Sign Out":
        FirebaseAuth.instance.signOut();
        Navigator.of(context).pop();
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPage(),
          ),
        );
        break;
    }
  }

  void pageChanged(int index) {
    setState(() {
      bottomIndex = index;
    });
  }

  void bottomTapped(int index) {
    setState(() {
      bottomIndex = index;
      _controller.animateToPage(index,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  @override
  void initState() {
    _getUser();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  _getUser() async {
    fuser = await FirebaseAuth.instance
        .currentUser()
        .then((user) => _setUser(user));
  }

  Future<FirebaseUser> _setUser(user) async {
    await Firestore.instance
        .collection("Users")
        .document(user.uid)
        .get()
        .then((snap) {
      setState(() {
        GeoPoint point = snap.data['location']['geopoint'];
        curLat = point.latitude;
        curLong = point.longitude;
        needsHelp = snap.data['case'];
      });
    });
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Covid-Kit",
            style: TextStyle(
              fontFamily: "FLORLRG",
            )),
        centerTitle: true,
        actions: <Widget>[
          PopupMenuButton<Choice>(
            onSelected: _select,
            itemBuilder: (BuildContext context) {
              return choices.map((Choice choice) {
                return PopupMenuItem<Choice>(
                  value: choice,
                  child: Text(choice.title),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: PageView(
        controller: _controller,
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: (index) {
          pageChanged(index);
        },
        children: <Widget>[
          GlobalCases(),
          CardPage(),
          NewsPage(),
          SuggestionsPage()
        ],
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: bottomIndex,
        showElevation: true,
        curve: Curves.easeInBack,
        onItemSelected: (index) {
          _controller.animateToPage(index,
              duration: Duration(milliseconds: 500), curve: Curves.ease);

          bottomTapped(index);
        },
        items: [
          BottomNavyBarItem(
            icon: Icon(FontAwesomeIcons.chartLine),
            title: Text('Stats'),
            activeColor: Colors.purpleAccent,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            icon: Icon(Icons.search),
            title: Text('Find'),
            activeColor: Colors.red,
            textAlign: TextAlign.center,
          ),
          
          BottomNavyBarItem(
            icon: Icon(Icons.new_releases),
            title: Text(
              'News',
            ),
            activeColor: Colors.blue,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            icon: Icon(Icons.lightbulb_outline),
            title: Text('Suggestions'),
            activeColor: Colors.yellow,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Switch Theme', icon: Icons.directions_car),
  const Choice(title: 'Sign Out', icon: Icons.directions_boat),
];
