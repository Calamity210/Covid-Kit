import 'package:Covid_Kit/locator.dart';
import 'package:Covid_Kit/login_api/authentication_service.dart';
import 'package:Covid_Kit/login_api/src/models/login_data.dart';
import 'package:flutter/foundation.dart';

enum ViewState { idle, busy }

class LoginModel extends ChangeNotifier {
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  ViewState _state = ViewState.idle;

  ViewState get state => _state;

  void setState(ViewState viewState) {
    _state = viewState;
    notifyListeners();
  }

  Future<bool> login(LoginData loginData) async {
    setState(ViewState.busy);

    var success = await _authenticationService.login(loginData);

    setState(ViewState.idle);
    return success;
  }

  // Future<bool> signup(LoginData loginData) async {
  //   setState(ViewState.busy);

  //   var success = await _authenticationService.signUp(loginData);

  //   setState(ViewState.idle);
  //   return success;
  // }
}
