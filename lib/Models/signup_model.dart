import 'package:Covid_Kit/locator.dart';
import 'package:Covid_Kit/login_api/authentication_service.dart';
import 'package:flutter/foundation.dart';
import 'package:geoflutterfire/geoflutterfire.dart';

enum ViewState { idle, busy }

class SignModel extends ChangeNotifier {
  final AuthenticationService _authenticationService =
      locator<AuthenticationService>();
  ViewState _state = ViewState.idle;

  ViewState get state => _state;

  void setState(ViewState viewState) {
    _state = viewState;
    notifyListeners();
  }

  Future<bool> signup(String email, String password, String name, String number,
  GeoFirePoint location, bool needs, String desc, String image ) async {
    setState(ViewState.busy);

    var success = await _authenticationService.signUp(email, password, name, number, location, needs, desc, image).then((suc){
    setState(ViewState.idle);
return suc;
    });

    return success;
  }
}
